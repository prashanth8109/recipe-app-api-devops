#!/bin/sh

set -e
#above command is to debug errors

python manage.py collectstatic --noinput
python manage.py wait_for_db 
# the above command will check if database is ready and then only proceed with remaining commands

python manage.py migrate

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

# uwsgi name of the application
# --socker :9000 run uWSGI as a TCP socket on port 9000
# --workers no of worker threads
# --master - run this as master service on the terminal. during shutdown docker gracefully terminates uwsgi service
# --enable-threads - to enable multi-threading
# --module app.wsgi is the actual application that uWSGI is going to run. its under the app folder in the code
